import csv
import sys
import sqlite3
import pymongo
from pymongo import MongoClient
from sqlite3 import Error


def Inicializacion():
    cluster = MongoClient("mongodb+srv://admin:1234@cluster0.w5yir.mongodb.net/Slang?retryWrites=true&w=majority")
    db = cluster["Slang"]
    collection = db["Slang"]
    post = collection.count_documents({})
    print(post)
    if post == 0:
        post1 = {"name": "que xopa", "desc": "Saludo"}
        post2 = {"name": "Queso", "desc": "Que es eso"}
        collection.insert_many([post1, post2])

def insertar(name, desc):
    cluster = MongoClient("mongodb+srv://admin:1234@cluster0.w5yir.mongodb.net/Slang?retryWrites=true&w=majority")
    db = cluster["Slang"]
    collection = db["Slang"]
    post1 = {"name": name , "desc": desc}
    collection.insert_one(post1)

def mostrar():
    cluster = MongoClient("mongodb+srv://admin:1234@cluster0.w5yir.mongodb.net/Slang?retryWrites=true&w=majority")
    db = cluster["Slang"]
    collection = db["Slang"]
    results = collection.find()
    for x in results: 
        print(x)

def buscar(name):
    cluster = MongoClient("mongodb+srv://admin:1234@cluster0.w5yir.mongodb.net/Slang?retryWrites=true&w=majority")
    db = cluster["Slang"]
    collection = db["Slang"]
    results = collection.find({"name": name})
    for results in results: 
        print(results["desc"])

def eliminar(name):
    cluster = MongoClient("mongodb+srv://admin:1234@cluster0.w5yir.mongodb.net/Slang?retryWrites=true&w=majority")
    db = cluster["Slang"]
    collection = db["Slang"]
    results = collection.delete_one({"name": name})

def actualizar(name, name2, desc):
    cluster = MongoClient("mongodb+srv://admin:1234@cluster0.w5yir.mongodb.net/Slang?retryWrites=true&w=majority")
    db = cluster["Slang"]
    collection = db["Slang"]
    results = collection.replace_one({"name": name}, {"name": name2, "desc": desc})
    


def main():
    Inicializacion()
    menu()
   


def menu():
    print("************ Diccionario de Slangs **************")
    print()

    choice = input("""
                      A: Agregar un Slang
                      B: Mostrar Slangs
                      C: Editar un Slang
                      D: Eliminar un Slag
                      F: Buscar descripción de un Slang
                      Q: Salir

                      Porfavor escoja una opción: """)

    if choice == "A" or choice =="a":
        name = input("Nombre del slang: ")
        desc = input("Descripción del slang: ")
        insertar(name, desc)
        menu()
    elif choice == "B" or choice =="b":
        mostrar()
        menu()
    elif choice=="C" or choice=="c":
        name = input("Escriba la palabra para editar: ")
        name2 = input("Nuevo nombre: ")
        desc = input("Nueva descripción: ")
        actualizar(name, name2, desc)
        menu()
    elif choice=="D" or choice=="d":
        name = input("Escriba el nombre de la palabra a borrar: ")
        eliminar(name)
        menu()
    elif choice=="F" or choice=="f":
        name = input("Escriba el nombre de la palabra para buscar su descripción: ")
        buscar(name)
        menu()
    elif choice=="Q" or choice=="q":
        sys.exit
    else:
        print("Debes escribir una letra")
        print("Porfavor intente de nuevo")
        menu()

main()
